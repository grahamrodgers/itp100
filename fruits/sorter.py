with open("unsorted_fruits_with_repeats.txt", "r") as f:
    sort = f.read().splitlines()
    sort.sort()
    myfile = open("sorted_fruit_counts.txt", "w")
    for item in sort:
        myfile.write('%s\n' % item)
    s = {}
    for word in sort:
        if word not in s:
            s[word] = 0
        s[word] += 1
        print(str(word) + ' shows up ' + str(s[word]) + ' times.')

