def grade_average(a_list):
    sum = 0
    for num in a_list:
        sum = sum + num
    average = sum / len(a_list)
    return average

a_list = [99, 100, 74, 63, 100, 100]
print(grade_average(a_list))
