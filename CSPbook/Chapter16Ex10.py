# setup the source list
source = ["This","is","a","list"]

# Set the accumulator to the empty list
so_far = []

# Loop through all the items in the source list
for index in range(0,len(source)):

    # Add a list with the current item from source to so_far
    so_far =  [source[index]] + so_far + [source[index]]
print(so_far)

