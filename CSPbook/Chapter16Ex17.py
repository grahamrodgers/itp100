def sumPos(theList):
    sum = 0
    for item in theList:
        if item >= 0:
            sum = sum + item
    return sum

print(sumPos([-4, 4, -1, 3, -2, -3, 1]))
