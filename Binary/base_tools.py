#Doctest 1

def to_binary(num):
    """
    Convert an decimal integer into a string representation of its binary
    (base2) digits.

      >>> to_binary(10) 
      '1010'
      >>> to_binary(12) 
      '1100'
      >>> to_binary(0) 
      '0'
      >>> to_binary(1) 
      '1'
    """

    return bin(num)

#Doctest 3

def to_base4(num):
    """
    Convert an decimal integer into a string representation of its base4
    digits.
      >>> to_base4(20)
      '110'
      >>> to_base4(28)
      '130'
      >>> to_base4(3)
      '3'
    """
    if num == 0:
        return [0]
    digits = []
    while num:
        digits.append(int(num % 4))
        num //= 4
    return str(digits[::-1])[1:-1].replace(", ", "")



if __name__ == '__main__':
    import doctest
    doctest.testmod()
