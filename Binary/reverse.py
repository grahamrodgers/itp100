def base64encode(three_bytes):
    """
      >>> base64encode('Wivm')
      'b'\\x5A\\x2B\\xE6''
      >>> base64encode('STOP')
      'b'\\x49\\x33\x8F''
      >>> base64encode('////')
      'b'\\xFF\\xFF\\xFF''
      >>> base64encode('AAAA')
      'b'\\x00\\x00\\x00''
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    try:

        ints = [digits.index(ch) for ch in three_bytes]
        b1 = (ints[0] << 2) | ((ints[1] & 48) >> 4)
        b2 = (ints[1] & 15) << 4 | ints[2] >> 2
        b3 = (ints[2] & 3) << 6 | ints[3]

        return bytes([b1, b2, b3])
        
    except (AttributeError, TypeError):
        raise AssertionError('Input should be 3 bytes')

    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'

if __name__ == '__main__':
    import doctest
    doctest.testmod()
