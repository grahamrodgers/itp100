def num_even_digits(n):
    """
    >>> num_even_digits(123456)
    3
    >>> num_even_digits(2468)
    4
    >>> num_even_digits(1357)
    0
    >>> num_even_digits(2)
    1
    >>> num_even_digits(20)
    2
    """
    nstr = str(n)
    length = len(nstr)
    res = 0;
    for digit in nstr:
        if (int(digit) % 2 == 0):
            res +=1
    return res

if __name__ == '__main__':
    import doctest
    doctest.testmod()

