import math
def is_prime(n):
    """
    >>> is_prime(2)
    True
    >>> is_prime(3)
    True
    >>> is_prime(4)
    False
    >>> is_prime(9)
    False
    >>> is_prime(243)
    False
    >>> is_prime(251)
    True
    """
    i=2
    while (i <= math.sqrt(n)):
        if ((n % i) == 0):
            return False
        i += 1
    return True

if __name__ == '__main__':
    import doctest
    doctest.testmod()

